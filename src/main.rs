extern crate exonum;

use std::fs::File;
use std::io::prelude::*;
use std::env;

use exonum::crypto::HexValue;

fn validator () {
    let mut validator_keys = File::create("validator_pub_keys.keys").unwrap();

    let (consensus_public_key, consensus_secret_key) =
        exonum::crypto::gen_keypair();
    let (service_public_key, service_secret_key) =
        exonum::crypto::gen_keypair();

    let mut keyfile = File::create(format!("keys1.keys")).unwrap();

    let pub_keys = [consensus_public_key, 
                    service_public_key];
    let priv_keys = [consensus_secret_key,
                     service_secret_key];

    for i in 0..2 {
        let _ = write!(keyfile, "{}\n{}\n", pub_keys[i].to_hex(), priv_keys[i].to_hex());
        let _ = write!(validator_keys, "{}\n", pub_keys[i].to_hex());
    }
}

fn auditor () {
    let (consensus_public_key, consensus_secret_key) =
        exonum::crypto::gen_keypair();
    let (service_public_key, service_secret_key) =
        exonum::crypto::gen_keypair();

    let mut keyfile = File::create(format!("keys1.keys")).unwrap();

    let pub_keys = [consensus_public_key, 
                    service_public_key];
    let priv_keys = [consensus_secret_key,
                     service_secret_key];

    for i in 0..2 {
        let _ = write!(keyfile, "{}\n{}\n", pub_keys[i].to_hex(), priv_keys[i].to_hex());
    }
}

fn main () {
    let args: Vec<String> = env::args().collect();

    //TODO: can be rewritten in a cute way
    if args.len() == 1 {
        println!("No keywords passed.\n    try --validator or --auditor");
    }
    else if args[1] == "--auditor" {
        auditor();
    }
    else if args[1] == "--validator" {
        validator();
    }
    else {
        println!(".{:?}.", args[1]);
        println!("Unknown keyword passed.\n    try --validator or --auditor");
    }
}
 